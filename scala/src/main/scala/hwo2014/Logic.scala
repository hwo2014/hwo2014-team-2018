package hwo2014

import hwo2014.model.MsgGameInit
import hwo2014.model.MsgGameEnd
import hwo2014.model.MsgCarPositions
import hwo2014.model.MsgPiecePosition
import hwo2014.model.MsgPiecePositionLane
import hwo2014.model.MsgCrash
import hwo2014.model._

case class PosInfo(
  dis : Float,
  lane : Int,
  speed : Float,
  angle : Float,
  angleSpeed : Float,
  angleAdd : Float
){}

class Logic{
	val name = "red tabby"

  var gameInit : MsgGameInit = null
  var carPositions : List[MsgCarPositions] = List[MsgCarPositions]()
  var crashAngles : List[Float] = List[Float]()
  var expectSpeedes : List[Float] = List[Float]()
  var expectSpeedAddes : List[Float] = List[Float]()
  var throttle = 0.7f

  val expectAcceleration = 0.2f
  val decelerationCoefficient = -0.1f
  val expectMaxSpeed = 10.0f

  //var crashAngle = 57.0f
  var crashAngle = 50.0f
  var nowSpeed = 0.0f
  var prevPieceIndex  = -1
  var prevSpeed = 0.f
  var crashFlg = false
  var debugPrint = false
  var prevDis = 0.f
  var nowPosInfo : PosInfo = null

  def init(_gameInit : MsgGameInit) = {
  	gameInit = _gameInit
    println(gameInit.getString)

    println("=====Piece Dis=====")
    (0 to gameInit.data.race.track.pieces.length-1).foreach(i => 
      println("pieceIndex," + i + ",dis," + gameInit.getDis(MsgPiecePosition(i,0,MsgPiecePositionLane(0,0),0)))
    )

    carPositions = List[MsgCarPositions]()
    crashAngles = List[Float]()

    expectSpeedes = gameInit.data.race.track.pieces.map(i => {
      if(i.isStraight()){
        expectMaxSpeed.toFloat
      }else{
        (expectMaxSpeed * 0.6f / i.radiusPerLength()).toFloat
      }
    })
    expectSpeedAddes = gameInit.data.race.track.pieces.map(i => {
      0.5f
    })

    //test
    //calcDis(true, 0, 0, 5)
    //debugPrint = true
    //calcDis(false, 0, 5, 0)
    //debugPrint = false
  }

  def crash(msgCrash : MsgCrash) = {
  	if(msgCrash.data.name == name){
	  	println("crash = " + carPositions.last.getCarPosition(name).get)

	  	//-----calc crashAngle-----//
			crashAngles = crashAngles ::: List(
				carPositions.last.getCarPosition(name) match {
					case Some(n) => n.angle
					case _ => 57.0.toFloat
				}
			)

			//val (s,l) = crashAngles.foldLeft((0.0,0))((t,r) => (t._1 + r,t._2+1))
			//crashAngle = (s / l).toFloat

      //-----reset expectSpeed-----//
      val opCarPosition = getNowCarPosition()
      if(opCarPosition != None){
        val nowCarPosition = opCarPosition.get
        val nowPieceIndex = nowCarPosition.piecePosition.pieceIndex
        var newSpeed = expectSpeedes(nowPieceIndex+1)-expectSpeedAddes(nowPieceIndex+1)
        newSpeed = if(newSpeed<0.f) 0.f else if(newSpeed > expectMaxSpeed) expectMaxSpeed else newSpeed

        expectSpeedes = expectSpeedes.updated(nowPieceIndex+1,newSpeed)
        expectSpeedAddes = expectSpeedAddes.updated(nowPieceIndex+1,expectSpeedAddes(nowPieceIndex+1)*0.8f)
        prevSpeed = 0
      }

      crashFlg = true

      nowPosInfo = PosInfo(nowPosInfo.dis,nowPosInfo.lane,0,0,0,0)
		}
  }

  def spawn() = {
    nowPosInfo = null //PosInfo(nowPosInfo.dis,nowPosInfo.lane,0,0,0,0)
  }

  def gameEnd(gameEnd : MsgGameEnd) = {
    //-----output speedes-----//
    println("expectSpeedes = List[Float](")
    expectSpeedes.foreach(i => {
      println(i + "f,")
    })
    println(")")


    println("expectSpeedAddes = List[Float](")
    expectSpeedAddes.foreach(i => {
      println(i + "f,")
    })
    println(")")
  }

  var debugFlg2 = false
  var testFlg = false
  def calcThrottle2() : Float = {
    if(nowPosInfo == null){
      1.0f
    }else{
      if(nowPosInfo.angleSpeed > 0){
        if(nowPosInfo.angleAdd > 0){
          if(!testFlg){
            //debugFlg2 = true
            testFlg = true
            val nowPiece : (Int,MsgGameInitPiece) = gameInit.getPiece(nowPosInfo.dis,nowPosInfo.lane)
            println("AngleSpeed,AngleAddが０じゃない時の計算式(PieceIndex=" + nowPiece._1)
          }else{
            debugFlg2 = false
          }
        }else{
          debugFlg2 = false
          testFlg = false
        }
      }else{
        debugFlg2 = false
        testFlg = false
      }

      if(consider(30,nowPosInfo)){
        getThrottleBySpeed(getExpectSpeed(nowPosInfo))
      }else{
        0.0f
      }
    }
  }

  def getExpectSpeed(posInfo : PosInfo) : Float = {
    val nowPiece : (Int,MsgGameInitPiece) = gameInit.getPiece(posInfo.dis,posInfo.lane)
    val expectSpeed = {
      if(nowPiece._2.isStraight){
        //次のピースも直線か?
        val nextPiece = gameInit.getPiece(nowPiece._1+1)
        if(nextPiece.isStraight){
          expectMaxSpeed              
        }else{//次のピースはカーブなのでカーブによって場合わけする
          val rate = nextPiece.radiusPerLength/1.2224152f
          val expectSpeed = 6.5f * rate

          val nowPieceDis = gameInit.getDis(MsgPiecePosition(nowPiece._1,0,MsgPiecePositionLane(posInfo.lane,posInfo.lane),0))
          val nextPieceDis = gameInit.getDis(MsgPiecePosition(nowPiece._1+1,0,MsgPiecePositionLane(posInfo.lane,posInfo.lane),0))

          val disRate = {
            val disRate = (posInfo.dis - nowPieceDis)/(nextPieceDis - nowPieceDis)
            if(disRate > 1.f){
              //println("*disRate oever 1.f:" + disRate)
              1.f
            }else{
              disRate
            }
          }

          throttleSpeed(dicisionThrottle(posInfo.speed, posInfo.dis, (expectSpeed-expectMaxSpeed) * disRate + expectMaxSpeed, posInfo.dis+posInfo.speed))
        }
      }else{
        val rate = nowPiece._2.radiusPerLength/1.2224152f
        6.5f * rate
        
        throttleSpeed(dicisionThrottle(posInfo.speed, posInfo.dis, 6.5f * rate, posInfo.dis+posInfo.speed))
      }
    }

    expectSpeed
  }

  def consider(cnt : Int, posInfo : PosInfo) : Boolean= {
    if(debugFlg2){
      println("posInfo=" + posInfo)
    }
    if(cnt==0){
      true
    }else{
      if(posInfo.angle.abs >= crashAngle){
        if(debugFlg2) println("consider crash")
        false
      }else{
        val expectSpeed = getExpectSpeed(posInfo)

        //-----速度計算-----//
        val add = calcAcceleration(posInfo.speed, expectSpeed)
        //-----アングル計算-----//
        val (newAngle,newAngleSpeed,newAngleAdd) = calcAngle(1, posInfo.angle, posInfo.angleSpeed, posInfo.angleAdd, 1.f, -0.118648331f, 0.00415566f) 

        consider(cnt-1,
          PosInfo(
            posInfo.dis + posInfo.speed,
            posInfo.lane,
            posInfo.speed + add,
            newAngle,
            newAngleSpeed,
            newAngleAdd
          )
        )
      }
    }
  }


  def calcThrottle() : Float = {
    if(gameInit != null){
      val opCarPosition = getNowCarPosition()
      if(opCarPosition != None){
        val nowCarPosition = opCarPosition.get
        val nowPieceIndex = nowCarPosition.piecePosition.pieceIndex
        val expectPieceIndex = if(nowPieceIndex+1 < expectSpeedes.length) nowPieceIndex+1 else 0
        val expectSpeed = if(expectPieceIndex < expectSpeedes.length){
          expectSpeedes(expectPieceIndex)
        }else{
          expectSpeedes(0)
        }
        val nextLap = if(nowPieceIndex > expectPieceIndex) nowCarPosition.piecePosition.lap+1 else nowCarPosition.piecePosition.lap

        val nowPieceDis = gameInit.getDis(MsgPiecePosition(nowPieceIndex,0,nowCarPosition.piecePosition.lane,nowCarPosition.piecePosition.lap))
        val nextPieceDis = gameInit.getDis(MsgPiecePosition(expectPieceIndex,0,nowCarPosition.piecePosition.lane,nextLap))

        if(nowPieceIndex != prevPieceIndex){
          println("==========ChangePiece==========")
          println("nowPieceIndex," + nowPieceIndex + ",nowSpeed," + nowSpeed)
          println("expectSpeedes(nowPieceIndex)," + expectSpeedes(nowPieceIndex))
          println("expectSpeedes(expectPieceIndex)," + expectSpeed)

          //-----reset expectSpeed-----//
          if(prevPieceIndex != -1){
            //if((expectSpeedes(prevPieceIndex)-prevSpeed).abs <= 0.1){//区分侵入速度が指定範囲内に入っていたら
            if(!crashFlg){
              var newSpeed = expectSpeedes(nowPieceIndex)+expectSpeedAddes(nowPieceIndex)
              newSpeed = if(newSpeed<0.f) 0.f else if(newSpeed > expectMaxSpeed) expectMaxSpeed else newSpeed

              expectSpeedes = expectSpeedes.updated(nowPieceIndex,newSpeed)
            //}
            }
          }

          crashFlg = false
          prevPieceIndex = nowPieceIndex
          prevSpeed = nowSpeed
        }

        val disRate = {
          val disRate = (nowDis - nowPieceDis)/(nextPieceDis - nowPieceDis)
          if(disRate > 1.f){
            //println("*disRate oever 1.f:" + disRate)
            1.f
          }else{
            disRate
          }
        }
        
        dicisionThrottle(nowSpeed,nowDis,(expectSpeed-expectSpeedes(nowPieceIndex)) * disRate + expectSpeedes(nowPieceIndex),nowDis+nowSpeed)
        //dicisionThrottle(nowSpeed,nowDis,expectSpeed,nextPieceDis)
      }else{
        0.0f
      }
    }else{
      0.0f
    }
  }

  def nowDis() = {
    val opCarPosition = getNowCarPosition()

    opCarPosition match {
      case Some(nowCarPosition) => gameInit.getDis(nowCarPosition.piecePosition)
      case _ => 0.0f
    }
  }

  def dicisionThrottle(nowSpeed : Float, nowDis : Float, expectSpeed : Float, expectDis : Float) = {
    val nowHigh : Float = (0 to 10).find(i => nowSpeed < throttleSpeed(i.toFloat * 0.1.toFloat)).getOrElse(10).toFloat / 10.f
    val expectHigh : Float = (0 to 10).find(i => expectSpeed < throttleSpeed(i.toFloat * 0.1.toFloat)).getOrElse(10).toFloat / 10.f 

    if(nowSpeed < expectSpeed){
      if(nowDis + calcDis(0,true, 0, nowSpeed, expectSpeed) > expectDis){//start add
        1.0f
      }else{
        nowHigh
      }     
    }else{
      if(nowDis + calcDis(0,false, 0, nowSpeed, expectSpeed) > expectDis){//start dec
        0.0f
      }else{
        nowHigh-0.1f
      }
    }
  }

  def addCarPosition(positions : MsgCarPositions){
  	carPositions = carPositions ::: List(positions)

  	//-----set nowSpeed-----//
    val opCarPosition = getNowCarPosition()

    opCarPosition match{
      case Some(nowCarPosition) => {
    		val nowDis = gameInit.getDis(nowCarPosition.piecePosition)

    		val prevCarPosition = getPrevCarPosition()

    		nowSpeed = prevCarPosition match{
    			case Some( n ) => 
    				val prevDis = gameInit.getDis(n.piecePosition)
            if(nowDis < prevDis){
              println("=====nowDisがマイナス=====")
              println("nowCarPosition=" + nowCarPosition.piecePosition)
              println("prevCarPosition=" + n.piecePosition)

              if(nowPosInfo==null){
                0.f
              }else{
                nowPosInfo.speed
              }
            }else{
              nowDis - prevDis  
            }
    			case _ => 
            if(nowPosInfo==null){
              0.f
            }else{
              nowPosInfo.speed
            }
    		}

        val nowAngleSpeed = prevCarPosition match{
          case Some( n ) => 
            nowCarPosition.angle - n.angle
          case _ => nowCarPosition.angle
        }

        val nowAngleAdd = nowAngleSpeed - (if(nowPosInfo==null) 0 else nowPosInfo.angleSpeed)

        nowPosInfo = PosInfo(
              nowDis,
              nowCarPosition.piecePosition.lane.startLaneIndex,
              nowSpeed,
              nowCarPosition.angle,
              nowAngleSpeed,
              nowAngleAdd
        )

        prevDis = nowDis
      }
      case _ => {
        nowPosInfo = PosInfo(0,0,0,0,0,0)
      }
    }
  }

  def getNowCarPosition() = {
    if(carPositions.length >= 1) carPositions.last.getCarPosition(name) else None
  }
  def getPrevCarPosition() = {
		if(carPositions.length >= 2) carPositions.reverse(1).getCarPosition(name) else None
  }

  def throttleSpeed(throttle : Float) : Float = {
  	(expectMaxSpeed*throttle).toFloat
  }
  def nowThrottleSpeed() : Float = {
  	throttleSpeed(throttle.toFloat)
  }
  def getThrottleBySpeed(speed : Float) : Float = {
    val lowSpeed = throttleSpeed(0)
    val highSpeed = throttleSpeed(1)

    (speed-lowSpeed) / (highSpeed-lowSpeed)
  }

  def calcDis(cnt : Int, up : Boolean, dis : Float, nowSpeed : Float, expectSpeed : Float) : Float = {
    if(debugPrint)
      println("up=" + up + " dis=" + dis + " nowSpeed=" + nowSpeed)

    if(cnt > 100){
      //println("*calcDis infinity loop up," + up + ",dis," + dis + ",nowSpeed," + nowSpeed + ",expectSpeed," + expectSpeed )
      dis
    }else{
      if(up){
        if(nowSpeed >= expectSpeed-0.2f) dis else calcDis(cnt+1,up,dis+nowSpeed,nowSpeed + calcAcceleration(nowSpeed,expectMaxSpeed),expectSpeed)
      }else{
        if(nowSpeed <= expectSpeed+0.2f) dis else calcDis(cnt+1,up,dis+nowSpeed,nowSpeed + calcAcceleration(nowSpeed,0),expectSpeed)
      }    
    }
  }

  def calcAcceleration(nowSpeed : Float, expectSpeed : Float) : Float = {
    if(nowSpeed < expectSpeed){
      //加速度=(engine power速度-現在速度)/engine power速度 * 予想加速度
      (expectSpeed-nowSpeed)/expectSpeed * expectAcceleration
    }else{
      //減速加速度=(engine power速度-現在速度)/(-予想最高速度/2)*減速加速度係数
      (expectSpeed-nowSpeed)/(-expectMaxSpeed * 0.5f) * decelerationCoefficient
    }
  }

  def nowAcceleration(){
    calcAcceleration(nowSpeed,nowThrottleSpeed)
  }

  //cnt分経過した時のangleを推測する
  def calcAngle(cnt : Int, nowAngle : Float, nowAngleSpeed : Float, baseAngleAdd : Float, angleAddRate : Float, angleAddRateDiff : Float, angleAddRateDiffDiff : Float) : (Float,Float,Float) = {
    if(cnt <= 0){
      (nowAngle,nowAngleSpeed,baseAngleAdd*angleAddRate)
    }else{
      val newAngleAddRateDiff = angleAddRateDiff + angleAddRateDiffDiff
      val newAngleAddRate = angleAddRate + angleAddRateDiff
      val newAngleAdd = baseAngleAdd*angleAddRate
      val newAngleSpeed = nowAngleSpeed + newAngleAdd
      val newAngle = nowAngle + newAngleSpeed

      calcAngle(
        cnt-1, 
        newAngle, 
        newAngleSpeed, 
        newAngleAdd,
        newAngleAddRate,
        newAngleAddRateDiff,
        angleAddRateDiff
      )
    }
  }  
}