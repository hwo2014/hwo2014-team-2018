package hwo2014.model

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec
//import hwo2014.MsgCommon
import hwo2014.model.MsgCarId

case class MsgGameEndTimeResult(
  laps : Int,
  ticks : Int,
  millis : Int
){}

case class MsgGameEndBestLap(
  car : MsgCarId,
  result : Option[MsgGameEndTimeResult]
){}

case class MsgGameEndResult(
  car : MsgCarId,
  result : Option[MsgGameEndTimeResult]
){}

case class MsgGameEndData(
  results : List[MsgGameEndResult],
  bestLaps : List[MsgGameEndBestLap]
){}

case class MsgGameEnd(
  msgType : String,
  data : MsgGameEndData
){}
