package hwo2014.model

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec
//import hwo2014.MsgCommon

case class MsgCrashData(
	name : String,
	color : String
){}

case class MsgCrash(
	msgType : String,
	data : MsgCrashData,
	gameId : String,
	gameTick : Int
){}