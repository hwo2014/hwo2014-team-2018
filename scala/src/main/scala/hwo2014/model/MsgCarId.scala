package hwo2014.model

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec

case class MsgCarId(
  name : String,
  color : String  
){}

