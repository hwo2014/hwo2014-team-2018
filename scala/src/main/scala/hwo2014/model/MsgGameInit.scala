package hwo2014.model

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec

import hwo2014.model.MsgCarId
import hwo2014.model.MsgCarPositions
import hwo2014.model.MsgPiecePosition
import hwo2014.model.MsgPiecePositionLane

case class MsgGameInitPiece(
  length : Option[Float],
  switch : Option[Boolean],
  radius : Option[Float],
  angle : Option[Float]
){
  def isStraight() : Boolean = {
    radius match{
      case Some(f) => false
      case None => true
    }
  }

  def getLength() = {
    if(isStraight){
      length.get
    }else{
      radius.getOrElse(0.0f) * 2.0f * 3.14f * (angle.getOrElse(0.0f).abs/380.0f)
    }
  }

  def radiusPerLength() = {
    if(isStraight()){
      0.f
    }else{
      radius.get / getLength()
    }
  }
}

case class MsgGameInitLane(
  distanceFromCenter : Float,
  index : Int
){}

case class MsgGamePosition(
  x : Float,
  y : Float
){}

case class MsgGameInitStartingPoint(
  position : MsgGamePosition,
  angle : Float
){}

case class MsgGameCarDimensions(
  length : Float,
  width : Float,
  guideFlagPosition : Float
){}

case class MsgGameInitTrack(
  id : String,
  name : String,
  pieces : List[MsgGameInitPiece],
  lanes : List[MsgGameInitLane],
  startingPoint : MsgGameInitStartingPoint
){
  def getLength(pieceIdx : Int,laneIdx : Int) : Float = {
    val piece = pieces(pieceIdx.toInt)
    val lane = lanes(laneIdx.toInt)

    if(piece.isStraight){
      piece.length.get
    }else{
      (piece.radius.getOrElse(0.0f)-lane.distanceFromCenter) * 2.0f * 3.14f * (piece.angle.getOrElse(0.0f).abs/380.0f)
    }
  }  

  def getLapDis(lane : Int) : Float = {
    (0 to pieces.length-1).foldLeft(0.0f)((sum,n) => sum + getLength(n,lane))
  }

}

case class MsgGameInitRaceSession(
  laps : Int,
  maxLapTimeMs : Int,
  quickRace : Boolean
){}

case class MsgGameInitCar(
  id : MsgCarId,
  dimensions : MsgGameCarDimensions
){}

case class MsgGameInitRace(
  track : MsgGameInitTrack,
  cars : List[MsgGameInitCar],
  raceSession : MsgGameInitRaceSession
){}

case class MsgGameInitData(
  race : MsgGameInitRace
){}

case class MsgGameInit(
  msgType: String,
  data : MsgGameInitData
){
  def getPiece(idx : Int) = {
    data.race.track.pieces(idx%data.race.track.pieces.length)
  }
  def getPiece(dis : Float, lane : Int) = {
    var sum = 0.f
    var pieceIndex = -1
    var i = 0

    var disOnLap = dis
    val lapDis = data.race.track.getLapDis(lane)
    while(disOnLap >= lapDis)disOnLap = disOnLap - lapDis

    data.race.track.pieces.foreach(piece => {
      sum = sum + data.race.track.getLength(i,lane)
      if(disOnLap < sum){
        if(pieceIndex == -1){
          pieceIndex = i
        }
      }
      i = i + 1
    })

    (pieceIndex,data.race.track.pieces(pieceIndex))
  }

  def getDis(piecePos : MsgPiecePosition) : Float= {
    if(piecePos.pieceIndex != 0){
      if(piecePos.pieceIndex < data.race.track.pieces.length){
        piecePos.lap * data.race.track.getLapDis(piecePos.lane.startLaneIndex) + (0 to piecePos.pieceIndex-1).foldLeft(0.0f)((sum,n) => sum + data.race.track.getLength(n,piecePos.lane.startLaneIndex)) + piecePos.inPieceDistance
      }else{//こないはず
        data.race.track.getLapDis(piecePos.lane.startLaneIndex)
      }
    }else{
      piecePos.lap * data.race.track.getLapDis(piecePos.lane.startLaneIndex)
    }
  }

  def getString() = {
    var ret = ""
    ret = ret + "laps = " + data.race.raceSession.laps + "\n"
    
    data.race.track.pieces.foreach(piece => {
      val lengthString = piece.length match {
          case Some(length) => "length="+length
          case None => ""
        }
        
      val switchString = piece.switch match {
          case Some(switch) => " switch="+switch
          case None => ""
        } 

      val radiusString = piece.radius match {
          case Some(radius) => " radius="+radius
          case None => ""
        } 
        
      val angleString = piece.angle match {
          case Some(angle) => " angle="+angle
          case None => ""
        }

      ret = ret + lengthString + switchString + radiusString + angleString + "\n"
    })    


    data.race.track.lanes.foreach(lane => {
      ret = ret + "lane(" + lane.index + ")," + lane.distanceFromCenter + "\n"
    })

    ret
  }
}


