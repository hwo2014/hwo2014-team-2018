package hwo2014.model

import org.json4s._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import scala.annotation.tailrec
//import hwo2014.MsgCommon
import hwo2014.model.MsgCarId

case class MsgPiecePositionLane(
  startLaneIndex: Int,
  endLaneIndex: Int
){}

case class MsgPiecePosition(
	pieceIndex : Int,
	inPieceDistance : Float,
	lane : MsgPiecePositionLane,
	lap : Int
){}

case class MsgCarPosition(
	id : MsgCarId,
	angle : Float,	//slip angle
	piecePosition : MsgPiecePosition
){
}

case class MsgCarPositions(
	msgType : String,
	data : List[MsgCarPosition],
	gameId : String,
	gameTick : Option[Int]
){
	def getCarPosition(name: String) = {
		data.find(carPos => carPos.id.name == name)
	}
}
