package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.util.Random

import hwo2014.model.MsgGameInit
import hwo2014.model.MsgGameEnd
import hwo2014.model.MsgCarPositions
import hwo2014.model.MsgCrash
import hwo2014.Logic

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  val name = "red tabby"

  val logic = new Logic

  var gameInit : MsgGameInit = null
  var carPositions : List[MsgCarPositions] = List[MsgCarPositions]()
  var throttle = 0.8
  var throttleTestCnt = 0
  var prevDiff = 0.0
  var sameCnt = 0
  var brake = false
  var brakeFrame = 30

  send(MsgWrapper("join", Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", _) =>
          logic.addCarPosition(Serialization.read[MsgCarPositions](line))
        case MsgWrapper("gameInit", data) =>
          println("Received: gameInit")
          logic.init(Serialization.read[MsgGameInit](line))
        case MsgWrapper("crash", _) =>
          println("Received: crash")
          logic.crash(Serialization.read[MsgCrash](line))
        case MsgWrapper("gameEnd",_) =>
          logic.gameEnd(Serialization.read[MsgGameEnd](line))
        case MsgWrapper("spawn",_) =>
          println("Received: spawn")
          logic.spawn()
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType)
      }

      //throttleTest()
      //angleTest()

      var throttle : Float = logic.calcThrottle2
      throttle = if(throttle > 1.0.toFloat) 1.0.toFloat else if(throttle < 0.toFloat) 0.toFloat else throttle

      send(MsgWrapper("throttle", throttle))
      play
    }
  }

  def angleTest(){
    val gameInit = logic.gameInit
    val carPositions = logic.carPositions
    val myCarPositionPrev = if(carPositions.length >= 2) carPositions.reverse(1).getCarPosition(name).getOrElse(null) else null

    if(myCarPositionPrev != null){    
      val myCarPosition = if(carPositions.length >= 1) carPositions.reverse(0).getCarPosition(name).getOrElse(null) else null
      if(myCarPosition != null){
        val nowDis = gameInit.getDis(myCarPosition.piecePosition)
        val prevDis = gameInit.getDis(myCarPositionPrev.piecePosition)
        val nowAngle = myCarPosition.angle
        val prevAngle = myCarPositionPrev.angle

        val diffAngle = nowAngle - prevAngle
        val diff = nowDis - prevDis
        val nowPiece = gameInit.getPiece(myCarPosition.piecePosition.pieceIndex)
        val length = gameInit.data.race.track.getLength(myCarPosition.piecePosition.pieceIndex,
          myCarPosition.piecePosition.lane.endLaneIndex)

        if(nowPiece.isStraight){
        }else{
          println("diff," + diff + ",angle," + myCarPosition.angle + ",pieceIndex," +  myCarPosition.piecePosition.pieceIndex + ",radiusPerLength," + (nowPiece.radius.get / length) + ",nowDis," + (myCarPosition.piecePosition.inPieceDistance / length))
        }    
      }
    }
  }

/*
  def throttleTest(){
    val myCarPositionPrev = if(carPositions.length >= 2) carPositions.reverse(1).getCarPosition(name).getOrElse(null) else null

    if(myCarPositionPrev != null){
      val myCarPosition = carPositions.last.getCarPosition(name).get
      val nowDis = gameInit.getDis(myCarPosition.piecePosition.pieceIndex, myCarPosition.piecePosition.inPieceDistance)
      val prevDis = gameInit.getDis(myCarPositionPrev.piecePosition.pieceIndex, myCarPositionPrev.piecePosition.inPieceDistance)
      val diff = nowDis - prevDis

      if(gameInit.getPiece(myCarPosition.piecePosition.pieceIndex).isStraight){
        throttleTestCnt += 1

        if(throttleTestCnt > brakeFrame){
          throttleTestCnt = 0
          throttle = if(brake) 1.0 else 0.2
          brake = if(brake) false else true

          var r = new Random
          brakeFrame = r.nextInt(60) + 20
        }

        //if(throttleTestCnt > 200){
          //throttle += 0.1
          throttle  = if(throttle > 1.0) 0.1 else throttle
          //throttleTestCnt = 0
          //prevDiff = 0
          //sameCnt = 0
        //}else{
          if(brake)
            if(diff != prevDiff){
              println("throttle," + throttle + ",diff," + prevDiff + ",sameCnt," + sameCnt + ",throttleTestCnt," + throttleTestCnt)
              prevDiff = diff
              sameCnt = 0
            }else{
              sameCnt += 1
            }
        //}
      }else{
        throttleTestCnt = 0
      }    
    }
  }
*/
  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}







